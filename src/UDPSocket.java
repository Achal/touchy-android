
import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.net.UnknownHostException;

public class UDPSocket {
	int server_port;
	InetAddress server_address = null;
	DatagramSocket socket = null;
	DatagramPacket packet = null;
	byte[] sendBuffer = null;
	byte[] recieveBuffer = null;
	
	public String init(String add, int port){
		server_port = port;
		try{
			server_address = InetAddress.getByName(add);
			socket = new DatagramSocket(port);
		}
		catch(UnknownHostException uhe){
			return "UnknownHostException"+ "Failed to resolve host: " +uhe.getMessage();
		}
		catch(SocketException se){
			return "SocketException"+ "Failed to create socket: " +se.getMessage();
		}
		return "initialised";
	}
	public String send(String msg){
		sendBuffer = msg.getBytes();
		packet = new DatagramPacket( sendBuffer, sendBuffer.length,server_address,server_port );
		try 
        {
			socket.send( packet );
        } 
        catch (IOException ioe) 
        {
            return "NETWORK"+ "Failed to send UDP packet due to IOException: " + ioe.getMessage();
        }
        catch( Exception e )
        {
            return "NETWORK"+ "Failed to send UDP packet due to Exeption: " + e.getMessage();
        }
		return "Packet Send";
	}
	public String recieve(){
		recieveBuffer = new byte[100];
		 packet = new DatagramPacket(recieveBuffer,recieveBuffer.length);
         try {
			socket.receive(packet);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			//e.printStackTrace();
			return e.toString();
		}
		return new String(recieveBuffer);
	}
	public DatagramPacket getRecievedPacket(){
		return this.packet;
	}
}
